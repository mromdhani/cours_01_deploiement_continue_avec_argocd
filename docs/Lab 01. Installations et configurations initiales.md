Le cluster OpenShift OKD de l'ULB continent une installation opérationnelle de ArgoCD dans le namespace `argocd` du cluster `DEV`. La console de ArgoCD est accessible à l'URL <https://argocd-server-argocd.apps.dev.okd.hpda.ulb.ac.be/>.

## Installation la CLI Argo CD 
La dernière version de la CLI Argi CD  est disponible en téléchargement à l'URL <https://github.com/argoproj/argo-cd/releases/latest>.

- **Installation sous Windows**  
  Pour installer la CLI sous Windows, il suffit de télécharger son exécutable pour Windows, à partir du lien indiqué ci-haut, le sauvegarder sur un répertoire de votre disque et l'ajouter le chemin de l’exécutable la variable PATH du système.
  
    Une manière alternative serait d'installer la CLI avec le gestionnaire packages **Chocolotey** de Windows. Référez-vous au lien suivant pour voir comment installer Chocolatey sous Windows : <https://docs.chocolatey.org/en-us/choco/setup#installing-chocolatey>.

    Pour installer Argo CD CLI, exécutez la commande suivante depuis la ligne de commande ou depuis PowerShell :
    ```bash
    choco install argocd-cli
    ```  

- **Installation sous Linux et WSL**    
  Télécharger le binaire de la CLI pour Linux. Lui ajouter le droit d'exécution. 

    ```bash
    curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
    chmod +x /usr/local/bin/argocd
    ```  
    Pour tester que la CLI s'est bien installée, exécuter la commande suivante
    ```bash
    argocd version
    ``` 

## Découverte de la console Web d'Argo CD
En plus de la CLI et de l'API, Argo CD fournit une interface Web conviviale. À l'aide de l'interface Web, vous pouvez obtenir une vue de haut niveau de toutes vos applications déployées sur plusieurs clusters ainsi que des informations très détaillées sur chaque ressource d'application. 

  - Ouvrez l'URL <https://argocd-server-argocd.apps.dev.okd.hpda.ulb.ac.be>;  s'authentifier, puis visualiser la liste des applications dans l'interface utilisateur de Argo CD.
  
    <figure markdown>
    ![console-web.](images/argocd-console-web.jpg){ width="auto" }
    <figcaption>Liste des applications Argo CD</figcaption>
    </figure>

La page de la liste des applications fournit des informations de haut niveau sur toutes les applications déployées, y compris l'état de santé et de synchronisation. En utilisant cette page, vous pouvez rapidement trouver si l'une de vos applications s'est dégradée ou a une dérive de configuration. L'interface utilisateur est conçue pour les grandes entreprises et capable de gérer des centaines d'applications. Vous pouvez utiliser la recherche et divers filtres pour trouver rapidement les applications souhaitées.

- Expérimentez avec les filtres et les paramètres d'affichage de page pour savoir quelles autres fonctionnalités sont disponibles dans la page de liste des applications.

Les informations supplémentaires sur l'application sont disponibles sur la page des détails de l'application. 

  - Accédez à la page des détails de l'application en cliquant sur la vignette de cette application. La page des détails de l'application visualise la hiérarchie des ressources de  l'application et fournit des détails supplémentaires sur la synchronisation et l'état de santé. Examinons de plus près l'arborescence des ressources de l'application et découvrons les fonctionnalités qu'elle fournit.
    <figure markdown>
    ![detail-application.](images/argocd-page-details.jpg){ width="auto" }
    <figcaption>Page de Détail d'une application ArgoCD</figcaption>
    </figure>
    
L'élément racine de l'arborescence des ressources est l'application elle-même. Le niveau suivant comprend les ressources gérées. Les ressources gérées sont des ressources définies par le manifeste dans Git et contrôlées explicitement par Argo CD. Les contrôleurs Kubernetes exploitent souvent la délégation et créent des ressources enfants pour déléguer le travail. La console web fournit des informations complètes sur chaque élément de l'application et fait de la page des détails de l'application un tableau de bord Kubernetes extrêmement puissant.


## Déclarer des clusters

- **Connectez-vous à Argo CD à l'aide de la CLI**
    
    En utilisant le nom d'utilisateur et le mot de passe qui vous ont été communiqués par l'administrateur, connectez-vous au serveur Argo CD en utilisant la CLI.
    ```bash
    argocd login https://argocd-server-argocd.apps.dev.okd.hpda.ulb.ac.be/ 
    ```

- **Enregistrer un cluster sur lequel déployer des applications**
  
    Le cluster qui vient configuré par défaut est le cluster local dans lequel ArgoCD est installé. C'est le cluster `DEV` dans le cas le cas de l'OKD de l'ULB. ArgoCD utilise le nom `in-cluster` pour le distinguer, et lui attribut l'adresse `https://kubernetes.default.svc`. C'est ce cluster qui est utilisé pour tout déploiement en interne.

    Commencez par lister tous les contextes de clusters dans votre `kubeconfig` actuel :
    ```bash
    oc config get-contexts -o name
    ```
    Choisissez un nom de contexte dans la liste et fournissez-le à argocd `cluster add CONTEXTNAME`. Par exemple, pour le contexte du cluster `STG` de l'ULB, exécutez :
    ```bash
    argocd cluster add https://api.stg.okd.hpda.ulb.ac.be:6443
    ```
    La commande ci-dessus installe un ServiceAccount ( `argocd-manager`), dans l'espace de noms `kube-system` de ce contexte `oc`, et lie le compte de service à un ClusterRole de niveau administrateur. Argo CD utilise ce jeton de compte de service pour effectuer ses tâches de gestion (c.-à-d. déploiement/surveillance).

    Le figure ci dessous montre la liste des clusters configurés pour l'Argo CD de OKD.
    
     <figure markdown>
      ![detail-application.](images/liste-clusters.jpg){ width="auto" }
      <figcaption>Liste des clusters configurés dans Argo CD</figcaption>
    </figure>


## Ajouter des dépôts Git
Si les manifestes Kubernetes de l'application à déployer se trouvent dans un dépôt Git privé, les informations d'authentification à ce dépôt doivent être configurées. Argo CD prend en charge les l'authentification Basic HTTP et SSH.

Les dépôts Git privés qui nécessitent un nom d'utilisateur et un mot de passe ont généralement une URL qui commence par `https://` plutôt que `git@` ou `ssh://`.

Les informations d'identification peuvent être configurées à l'aide de l'interface de ligne de commande Argo CD :

```bash
argocd repo add https://github.com/argoproj/argocd-example-apps --username <username> --password <password>
```

Une fois ajouté avec succès, un dépôt Git apparaitra dans la liste des dépôts de la console Web.   

## Setup d'une environnement de développement VS Code pour Argo CD

Visual Studio Code, connu aussi sous le nom raccourci VS Code, est un éditeur de code extensible développé par Microsoft pour Windows, Linux et macOS. Visual Studio Code.  VS Code prend en compte plusieurs langages de programmation via une panoplie d'extensions disponibles sur son "[Marketplace](https://marketplace.visualstudio.com/vscode)". 

Avec la fonctionnalité « IntelliSense », VS Code assiste les développeurs tout au long de leurs développements afin de simplifier l'édition du code. VSCode vous permet également d'interagir avec les clusters Kubernetes depuis son interface soit via le terminal intégré dans VS code ou bien via les fonctionnalités de l'exention. 

- **Installation de VS Code sous Windows**
  
    La mise en place et l'utilisation de Visual Studio Code est rapide et facile. Il s'agit d'un petit téléchargement qui vous permet de l'installer en quelques minutes.

    - Téléchargez le [programme d' installation de Visual Studio Code](https://go.microsoft.com/fwlink/?LinkID=534107) pour Windows.
    - Une fois téléchargé, exécutez le programme d'installation (VSCodeUserSetup-{version}.exe).
    !!! Note
        Le programme d'installation ajoutera Visual Studio Code à votre fichier %PATH%. Ainsi, à partir de la console, vous pourrez saisir "code". pour ouvrir VS Code sur ce dossier. Vous devrez redémarrer votre console après l'installation pour que la modification de la %PATH%variable d'environnement prenne effet.
    Il est possible d'installer également VS Code avec le gestionnaire packages **Chocolotey** de Windows. Référez-vous au lien suivant pour voir comment installer Chocolatey sous Windows : <https://docs.chocolatey.org/en-us/choco/setup#installing-chocolatey>.
    ```bash
    choco install vscode
    ```  

- **Installation de VS Code sous Linux**
  
    Pour installer VS Code sous les distributions Linux basées sur Debian et Ubuntu, exécutez d'abord les instructions suivantes pour télécharger le dépôt là ou VS Code réside et la clé d'intégrité GPG.
    ```bash
    sudo apt-get install wget gpg
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
    rm -f packages.microsoft.gpg
    ```

    Ensuite, mettez à jour le cache du package et installez VS Code en utilisant :

    ```
    sudo apt install apt-transport-https
    sudo apt update
    sudo apt install code 
    ```
    Pour l'installation sur les autres distributions de Linux, consulter les détails ic : <https://code.visualstudio.com/docs/setup/linux>.

- **Installation l'extension Kubernetes pour VS Code**

    Pour une expérience Kubernetes entièrement intégrée, vous pouvez installer l' extension [Kubernetes Tools](https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools) , qui vous permet de développer rapidement des manifestes Kubernetes.

    Pour installer l'extension Kubernetes, procéder comme suit: 
    
    - Ouvrez la vue Extensions ( `Ctrl+Maj+X `) à partir du volet de gauche de l'IDE
    - Recherchez "kubernetes". 
    - Sélectionnez l'extension Microsoft Kubernetes, et l'installer

    <figure markdown>
      ![detail-application.](images/kubernetes-vscode-extension.webp){ width="auto" }
      <figcaption>Extension Kubernetes pour VS Code</figcaption>
    </figure>

- **Installation l'extension YAML de RedHat pour VS Code**

    [L'extension YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) fournit une prise en charge complète du langage YAML pour Visual Studio Code. Elle permet notamment le formatage et la validation des documents YAML. 

    Pour installer l'extension Kubernetes, procéder comme suit: 
    
    - Ouvrez la vue Extensions ( `Ctrl+Maj+X `) à partir du volet de gauche de l'IDE
    - Recherchez "Yaml". 
    - Sélectionnez l'extension Yaml de RedHat, et l'installer

    <figure markdown>
      ![detail-application.](images/yaml-vscode-extension.webp){ width="auto" }
      <figcaption>Extension Yaml pour VS Code</figcaption>
    </figure>