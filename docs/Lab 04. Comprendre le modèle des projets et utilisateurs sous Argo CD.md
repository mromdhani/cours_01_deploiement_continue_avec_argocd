## Scénario
On suppose que l'on a deux deux équipes nommées respectivement `droit` et `archi`. Chaque équipe a ses propres applications déployées, via ArgoCD, dans un namespace dédiée, soient respectivement `f-droit` et `f-archi`. L'objectif est ici de mettre en place une configuration de ArgoCD de manière à garantir que ces deux équipes travaillent de manière isolée. Ils peuvent ainsi gérer librement leurs applications, c'est à dire celles qui résident dans le namespace approprié, mais ne doivent aucun cas accéder aux applications de l'équipe voisine.


## Utilisateurs, Rôles, et RBAC

ArgoCD adopte un modèle RBAC pour configurer les autorisations accordées aux utilisateurs. Ainsi, les autorisations sont configurées via des rôles et sont définies par un ensemble de politiques (policy) décrivant les objets à autoriser et les opérations que les utilisateurs peuvent effectuer sur ces objets. Ces politiques d'accès peut être configurées soit globalement, par cluster, ou de manière ou spécifiques à certains projets. Nous verrons un peu loin ce qui est un projet au sens ArgoCD.

Les utilisateurs sont configurées dans une configMap appelée `argocd-cm` et les politiques RBAC sont configurées dans une configMap appelée `argocd-rbac-cm`. Ces deux configMaps sont disponibles dans le namespace d'installation par défaut d'ArgoCD, à savoir `argocd`. 

Avec cela, l'accès peut être configuré globalement par cluster ou dédié aux projets .

- Pour lister le contenu par défaut de la configMap `argocd-cm`, exécuter la commande suivante:
  ```bash linenums="0" 
  kubectl get configmap argocd-cm -n argocd -o yaml
  ```
- Pour lister le contenu par défaut de la configMap `argocd-rbac-cm`, exécuter la commande suivante:
  ```bash linenums="0" 
  kubectl get configmap argocd-rbac-cm -n argocd -o yaml
  ```

## Création des utilisateurs

Pour créer un nouvel utilisateur, on doit modifier la configMap `argocd-cm`. Bien entendu, on doit être un administrateur d'ArgoCD pour réaliser ce type d'opérations.

- Obtenir une copie de la configMap actuelle
  ```bash linenums="0" 
  kubectl get configmap argocd-cm -n argocd -o yaml > argocd-cm.yml
  ```
- Apportez maintenant les modifications suivantes au ConfigMap pour créer les deux utilisateurs `droit` et `archi`:
  ```
  data : 
    accounts.droit : apiKey, login
    accounts.archi : apiKey, login
  ```
  Cela ajoutera deux nouveaux utilisateurs, nommés `droit` et `archi`, qui peuvent se connecter soit via une clé API ou via un login à partir de la CLI ou à partir de la console web d'ArgoCD.

- Appliquez les modifications en exécutant :
  ```bash linenums="0" 
  kubectl apply -f argocd-cm.yml
  ```
- Vérifer les utilisateurs ont été bel et bien créés.
  ```bash linenums="0" 
  argocd account list
  ```
- Mettre à jour, maintenant, le mot de passe des utilisateurs.
  ```bash linenums="0"
  argocd update-password --account <username> --new-password <new-password>
  ```
  Cela vous demandera le mot de passe actuel. Ce sera le mot de passe de l'utilisateur de l'utilisateur admin.

## Mise à jour de la politique RBAC pour les utilisateurs

Par défaut, un utilisateur fraichement créé peut se connecter avec un accès en lecture seule. La configMap `argocd-rbac-cm` doit être mise à jour. 
- Extraire le contenu de la configMap actuelle :
  ```bash linenums="0"
  kubectl get configmap argocd-rbac-cm -n argocd -o yaml > argocd-rbac.yml
  ```
- D'abord ajouter les utilisateurs aux rôles. Ceci est nécessaire car les autorisations RBAC font référence à ces rôles quand ils sont définies:
   ```yaml
    # Add users to roles
    g, droit, role:droit-role
    g, archi, role:archi-role
   ```
  Les lignes ci-dessus déclarent les utilisateurs `droit` et `archi` comme membre des rôles respectifs `droit-role` et `archi-role`. Chaque ligne commence par la lettre `g` et cela signifie qu'il s'agit d'une policy du type groupe.

- Ensuite accordons les autorisations aux rôles. On utilise la lettre `p` au début de chaque ligne pour indiquer qu'une s'agit d'une policy RBAC.
  ```yaml
  # Manage applications ONLY under f-droit namespace
  p, role:droit-role, applications, *, f-droit/*, allow

  # View existing clusters
  p, role:droit-role, clusters, get, *, allow    

  # Manage repositories
  p, role:droit-role, repositories, *, *, allow
  ```
    * La ligne numéro `2` accorde aux role `droit-role` tous les droits de gérer les ressources de type `application` dans le namespace `f-droit` seulement. Il peut ainsi créer, consulter, éditer et supprimer des applications dans ce namespace.
    * La ligne numéro `5` accorde aux role `droit-role` seulement les droits de consultation de la liste des clusters.
    * La ligne numéro `85` accorde aux role `droit-role` tous les droits de gérer les ressources de type `repository` dans ArgoCD. Il peut ainsi inscrire de nouveaux dépots Git dans ArgoCD, consulter, éditer ou supprimer des dépots existants.

## Les projets ArgoCD

L'utilité des projets dans ArgoCD est de permettre de spécifier l'accès à des namespaces, des dépots Git,des clusters, etc. Ainsi nous pourrons via des projets limiter chaque équipe de développeurs d'accéder seulement à son namespace.

A l'installation ArgoCD créé le projet default
```bash linenums="0" 
argocd proj list
```

## Création d'un nouveau projet ArgoCD

Un projet ArgoCD peut être créé avec un manifeste Yaml comme celui-ci :

```yaml
kind: AppProject
metadata:
  name: projet-f-droit
  namespace: argocd
spec:
  clusterResourceWhitelist:
  - group: '*'
    kind: '*'
  destinations:
  - namespace: f-droit
    server: https://kubernetes.default.svc
  orphanedResources:
    warn: false
  sourceRepos:
  - '*'
```
Le manifeste Yaml ci-dessus créé un projet ArgoVD nommé  `projet-f-droit`. Ce projet couvre l'activité sur le namespace `f-droit` du cluster par défaut (`https://kubernetes.default.svc`).

On peut également créer un projet avec la CLI de ArgoCD. La commande CLI suivante produit le même effet que le manifeste Yaml ci-dessus.

```bash linenums="0" 
argocd proj create projet-f-droit -d https://kubernetes.default.svc,f-droit 
```

Pour vérifier que le projet a été bien créé, listons de nouveaux les projets
```linenums="0" 
argocd proj list
```

## Les projects ArgoCD et les Roles

L'accès aux projets est configuré via la configMap  `argocd-rbac-cm`. Le configuration suivante accorde aux membres du groupe `doit-role` de droit de gérer les applications relevant du projet ArgoCD `projet-f-droit`.

```yaml
# Manage applications ONLY under projet-f-droit project
  p, role:droit-role, applications, *, projet-f-droit/*, allow
```

Dans le cas où l'on veuille assurer que les rôles `droit-role` et `archi-role` travaillent de manière isolée ; on créé un projet ArgoCD  et un groupe pour chaque équipe ainsi qu'une règle d'autorisation comme limite le rôle au projet concerné.

## Récapitulation sur la gestion des utilisateurs et des rôles

Voici un récapitulatif des configurations pour mettre en place un

1. Créer les utilisateurs en les ajoutant dans la configMap `argocd-cm`. Assigner des mots de passes initiaux à ces utilisateurs avec la commande `argocd update-password`.
2. Créer les groupes et inscrire les utilisateurs créés dans l'étape 1. à ces groupes. Les groupes sont créés en les ajoutant à la configMap `argocd-rbac-cm` ; pour chaque groupe une nouvelle policy du type `g` est ajoutée.
3. Créer des projets ArgoCD en précisant pour chaque projet le clusters,  les namespaces, et éventuellement les dépots Git. Les projets peuvent être créés soit via le Yaml ou via la CLI argocd.
4. Ajouter les règles d'accès RBAC à la configMap  `argocd-rbac-cm` de manière à déclarer quel rôle accède à quel projet ArgoCD. Pour chaque une policy de type `p` est ajoutée à la configMap `argocd-rbac-cm`. 