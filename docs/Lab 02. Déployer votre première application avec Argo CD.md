
## Scénario
On dispose d'une application du type `'Hello World'`appelée **Guestbook**. Elle fait partie des exemples d'applications d'apprentissage d'Argo CD listées ici <https://github.com/argoproj/argocd-example-apps>. L'application est disponible en plusieurs versions : fichiers Yaml, Chart Helm, Kustomize, ...

Voici le dépôt Git de cette application en version Yaml: <https://github.com/argoproj/argocd-example-apps/tree/master/guestbook>. On y retrouve les deux descripteurs Yaml suivants:

1. `guestbook-ui-deployment.yaml`: le manifeste du déploiement de l'application. 
2. `guestbook-ui-svc.yaml`: le manifeste du service de l'application.  

L'objectif de ce lab est de procéder au déploiement de cette application en utilisant différents techniques.


## Déploiement avec la CLI de ArgoCD
La première méthode consiste à utiliser la CLI d'ArgoCD.

- Se logger sous Argocd Server
    ```  bash
  argocd login https://argocd-server-argocd.apps.dev.okd.hpda.ulb.ac.be/
  ```
- Exécuter la commande CLI suivante
    ```bash
    argocd app create guestbook \
        --repo https://github.com/argoproj/argocd-example-apps.git \
        --path guestbook \
        --dest-server https://kubernetes.default.svc \
        --dest-namespace default
        --project myproject
    ```
  L'exécution de cette commande crée la ressource `Application` de ArgoCD puis déploie cette application dans le namespace default du cluster OpenShift par ArgoCD.

## Déploiement avec les manifestes Yaml de ArgoCD

Pour déployer une application avec l'approche déclarative, on utilise la CRD `application` d'Argo CD. Les informations essentielles sont:
- `source` : référence à l'état souhaité dans Git (dépôt, révision, chemin, environnement)
- `destination`: référence au cluster cible et à l'espace de noms. Pour le cluster, un serveur ou un nom peut être utilisé, mais pas les deux (ce qui entrainera une erreur). Sous le capot, lorsque le serveur est manquant, il est calculé en fonction du nom et utilisé pour toutes les opérations. Préciser le nom du projet si l'application appartient à un projet donné.
!!! Note
    Ce mode d'installation est à utiliser seulement dans des scénarios de tests ou de prototypage rapide. 

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: guestbook
spec:
  project: myproject
  source:
    repoURL: https://github.com/argoproj/argocd-example-apps.git
    targetRevision: HEAD
    path: guestbook
  destination:
    server: https://kubernetes.default.svc
    namespace: default
```
!!! Note
    Ce mode d'installation est le mode recommandé pour les applications métier de l'ULB. Il permet de partager, de reproduire et de versionner les options du déploiement.

## Déploiement avec la console Web de ArgoCD

Pour déployer une application en utilisant la console web de Argo CD, lancer votre navigateur préféré et connectez-vous sur la console Web. Cliquez sur le bouton **+ NEW APP** comme indiqué ci-dessous :
<figure markdown>
![detail-application.](images/new-app.png){ width="auto" }
<figcaption>Création d'une nouvelle application</figcaption>
</figure>
Introduire ensuite le nom de l'application, le dépôt git source, le cluster et le namespace de destination. 
!!! Note
    Ce mode d'installation est à utiliser seulement dans des scénarios de tests ou de prototypage rapide.   