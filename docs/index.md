# Bienvenue dans ArgoCD

## Qu'est ce que ArgoCD ?

[Argo CD](https://argo-cd.readthedocs.io/en/stable/) est un opérateur GitOps open source pour Kubernetes.  Le projet fait partie de la famille Argo, un ensemble d'outils cloud natifs pour exécuter et gérer des tâches et des applications sur Kubernetes. Avec Argo Workflows, Rollouts et Events, Argo CD se concentre sur les cas d'utilisation de la livraison d'applications et facilite la combinaison de trois modes de calcul : les services, les workflows et le traitement basé sur les évènements. En 2020, Argo CD a été accepté par la Cloud Native Computing Foundation(CNCF) en tant que projet hébergé au niveau de l'incubation.

Argo CD suit le modèle GitOps d'utilisation des dépôts Git comme source de vérité pour définir l'état souhaité d'une application(le "desired state"). Le principal avantage de GitOps est que chaque modification est versionnée et vérifiable. Le versioning permet de revenir facilement à un état précédent en cas d'erreur. La reprise après sinistre est également simplifiée. La source de vérité reste inchangée et il vous suffit de changer d’environnement ciblé. ArgoCD offre une Web UI qui permet de visualiser tous les objets gérés par ArgoCD ainsi que leur état actuel. 

<figure markdown>
![argocd-wrokflow.](images/argocd-workflow.png){ width="500px" }
<figcaption>ArgoCD synchronise Openshift par rapport à un état décrit sur Git</figcaption>
</figure>


!!! Note
    CNCF La Cloud Native Computing Foundation est un projet de la Linux Foundation qui héberge des projets liés à l'art de la conteneurisation.


## Composants d'ArgoCD

ArgoCD se compose des éléments suivants. Il est possible de visualiser ces composants en explorant le projet `argocd` d'Openshift OKD de l'ULB.

- **argocd-server** : Il s'agit d'un `Deployment` au sens Kubernetes; il représente le serveur principal d'ArgoCD.
- **argocd-application-controller**: Il s'agit d'un `StatefulSet` au sens Kubernetes; il représente le contrôleur d'ArgoCD.
- **argocd-redis**: Il s'agit d'un `Deployment` au sens Kubernetes; c'est la base de données Cle/Valeur dans laquelle ArgoCD stocke les données.
- **argocd-dex-server**: Il s'agit d'un `Deployment` au sens Kubernetes; il est en charge de lier ArgoCD aux fournisseurs d'identité OpenID Connect (OIDC)[^1].

[^1]: On peut considérer Dex comme un intermédiaire entre ArgoCD et des fournisseurs d'identité externes qui implémentent le standard d'authentification OpenID Connect comme Okta, GitHub, Google, Microsoft et LinkedIn, entre autres.

<figure markdown>
![argocd](images/composants-argocd.jpg){ width="600px" }
<figcaption>Composants d'ArgoCD</figcaption>
</figure>

## Se connecter sur le serveur ArgoCD de l'ULB

Voici les informations concernant sur le serveur ArgoCD de l'ULB.

- Projet OpenShift: `argocd`
- URL d'accès : <https://argocd-server-argocd.apps.dev.okd.hpda.ulb.ac.be/>

La page d'accueil d'ArgoCD se présente comme suit:


<figure markdown>
![argocd](images/login-argocd.jpg){ width="600px" }
<figcaption>Page de Login d'ArgoCD</figcaption>
</figure>

Pour se connecter sur ArgoCD, obtenir votre combinaison de username/password auprès de l'administrateur ArgoCD. Il vous sera possible de changer votre mote de passe par la suite.