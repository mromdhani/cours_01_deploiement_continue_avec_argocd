
## Scénario
L'objectif ici est de se familiariser avec les commandes usuelles de la CLI ArgoCD.


## Commandes usuelles de la CLI Argo CD

- Avoir l'aide de la commande argocd
  ```bash linenums="0" 
  argocd -h
  ```
- Se logger sur une instance de serveur argocd 
  ```bash  linenums="0" 
  argocd login <ArgoCD_Server>
  ```
- On se délogge de la session courante avec la commande:
  ```bash linenums="0" 
  logout
  ```
- Changer son mot de passe:
  ```bash linenums="0" 
  argocd account update-password
  ```
- Inscrire un dépôt GIT dans le serveur ArgoCD
  ```bash linenums="0" 
 argocd repo add <Repo_URL> --username <username> --password <password>
  ```
  Si vous utiliser une authentification SSH, vous devez indiquer votre clé privée SSH pour que ArgoCD puisse la présenter pour s'authentifier sur Git 
  ```bash linenums="0" 
argocd repo add <Repo_SSH_URL> --ssh-private-key-path <Path_TO_Your_ssh_key> 
  ```
- Inscrire un cluster
  ```bash linenums="0" 
  argocd cluster add CONTEXT # CONTEXT ici est un contexte de connexion au sens Kubernetes/OpenShift.
  ```

- Création d'une application ArgoCD
  ```bash linenums="0" 
      argocd app create <Application_Name> \
        --repo <Git_Repo_URL> \
        --path <Chemin_dans_Git_Repo> \
        --dest-server <Destination_Cluster> \
        --dest-namespace <Destination_Namespace> 
       --project <Destination_Project> 
   ```

- Lister les applications installées
  ```bash linenums="0" 
      argocd app list 
   ```
- Avoir les détails sur l'état d'une application déployée
  ```bash linenums="0" 
  argocd app get <APP_Name>
  ```

- Faire un Sync pour actualiser le déploiement d'une application
  ```bash linenums="0" 
  argocd  app sync <APP_Name>
  ```
## Aller plus loin avec les commandes argocd

Voici un extrait de l'aide de la commande argocd.

```bash  linenums="0" 
argocd controls a Argo CD server

Usage:
  argocd [flags]
  argocd [command]

Available Commands:
  account     Manage account settings
  admin       Contains a set of commands useful for Argo CD administrators and requires direct Kubernetes access
  app         Manage applications
  cert        Manage repository certificates and SSH known hosts entries
  cluster     Manage cluster credentials
  completion  output shell completion code for the specified shell (bash or zsh)
  context     Switch between contexts
  gpg         Manage GPG keys used for signature verification
  help        Help about any command
  login       Log in to Argo CD
  logout      Log out from Argo CD
  proj        Manage projects
  relogin     Refresh an expired authenticate token
  repo        Manage repository connection parameters
  repocreds   Manage repository connection parameters
  version     Print version information
```